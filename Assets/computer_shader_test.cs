﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class computer_shader_test : MonoBehaviour
{
    public int SphereAmount = 17; //創造幾個數量
    public ComputeShader Shader; //ComputeShader引用欄位

    ComputeBuffer resultBuffer;
    int kernel;
    uint threadGroupSize;
    Vector3[] output;
    public static bool supportsComputeShaders;
    public GameObject Prefab;//球引用
    Transform[] instances;
    // Start is called before the first frame update
    void Start()
    {
       //program we're executing
        kernel = Shader.FindKernel("Spheres");
        Shader.GetKernelThreadGroupSizes(kernel, out threadGroupSize, out _, out _);

        //buffer on the gpu in the ram
        resultBuffer = new ComputeBuffer(SphereAmount, sizeof(float) * 3);
        output = new Vector3[SphereAmount];

        // in start method

        //spheres we use for visualisation
        instances = new Transform[SphereAmount];
        for (int i = 0; i < SphereAmount; i++)
        {
            instances[i] = Instantiate(Prefab, transform).transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Shader.SetBuffer(kernel, "Result", resultBuffer);
        int threadGroups = (int) ((SphereAmount + (threadGroupSize - 1)) / threadGroupSize);
        Shader.Dispatch(kernel, threadGroups, 1, 1);
        resultBuffer.GetData(output);


        //in update method
        for (int i = 0; i < instances.Length; i++)
        {
            instances[i].localPosition = output[i];
        }

    }

     void OnDestroy()
    {
        resultBuffer.Dispose();
    }
}
