﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pp : MonoBehaviour
{
   public ComputeShader ComputeShader;
   public int  sizeCount;
   struct Data {
        public float A;
        public float B;
        public float C;
   }
   
    // Start is called before the first frame update
    void Start()
    {
        System.Diagnostics.Stopwatch time = new System.Diagnostics.Stopwatch ();
        time.Start ();
        Data [] inputData = new Data[sizeCount];
        Data [] outputData = new Data[sizeCount];
        print ("輸入 --------------------------------------------");
        for(int i=0; i <sizeCount; i++){
            inputData[i].A = i * 3 + 1;
            inputData[i].B = i * 3 + 2;
            inputData[i].C = i * 3 + 3;
           // print (inputData[i].A + ", " + inputData[i].B + ", " + inputData[i].C);
        }

        // Data 有3個float，一個 float 有 4 Byte，所以 3 * 4 = 12
        ComputeBuffer inputbuffer = new ComputeBuffer(sizeCount, 12);
        ComputeBuffer outputbuffer = new ComputeBuffer(sizeCount, 12);


        int k =ComputeShader.FindKernel ("CSMain");

        // 寫入 GPU
        inputbuffer.SetData(inputData);//從array放到buffer
        ComputeShader.SetBuffer(k, "inputData", inputbuffer);

        // 計算，並輸出至 CPU
        ComputeShader.SetBuffer(k, "outputData", outputbuffer);
        ComputeShader.Dispatch(k, sizeCount, 1, 1);
        outputbuffer.GetData (outputData);//從buffer放到array儲存

        print ("輸出 --------------------------------------------");
        // 打印結果
        for(int i=0; i < sizeCount; i++){           
           // print(outputData [i].A + ", " + outputData [i].B + ", " + outputData [i].C);
        }

        // 釋放
        inputbuffer.Dispose ();
        outputbuffer.Dispose ();
        time.Stop ();
        print("gpu執行 " + time.Elapsed.TotalSeconds + " 秒");
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
