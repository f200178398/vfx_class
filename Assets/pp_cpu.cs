﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pp_cpu : MonoBehaviour
{
    // Start is called before the first frame update
    
    public int  sizeCount;
   struct Data {
        public float A;
        public float B;
        public float C;
   }
    void Start()
    {
        System.Diagnostics.Stopwatch time = new System.Diagnostics.Stopwatch ();
        time.Start ();
        Data [] inputData = new Data[sizeCount];
        Data [] outputData = new Data[sizeCount];
        print ("輸入 --------------------------------------------");
         for(int i=0; i <sizeCount; i++){
            inputData[i].A = i * 3 + 1;
            inputData[i].B = i * 3 + 2;
            inputData[i].C = i * 3 + 3;
           // print (inputData[i].A + ", " + inputData[i].B + ", " + inputData[i].C);
        }
         print ("輸出 --------------------------------------------");
        for(int i=0; i < sizeCount; i++){         
             outputData[i].A = inputData[i].A * 2;
             outputData[i].B = inputData[i].B * 2;
             outputData[i].C = inputData[i].C * 2;  
           // print(outputData [i].A + ", " + outputData [i].B + ", " + outputData [i].C);
        }
        time.Stop ();
        print("CPU執行 " + time.Elapsed.TotalSeconds + " 秒");
    }



}
