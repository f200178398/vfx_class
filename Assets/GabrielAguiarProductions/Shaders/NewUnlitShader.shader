﻿Shader "Unlit/NewUnlitShader"
{
  Properties{
       _Stencil ("Stencil ID", int) = 0// StencilLightingUsage.RegularLighting
       _StencilComp ("StencilComp", Int) =  1 //3 = equal
  }

   SubShader {

        Tags { "RenderType"="Opaque" "Queue"="Geometry"}

        Pass {
            // Only render pixels whose value in the stencil buffer equals 1.
           Stencil {
               Ref 1
               Comp [_StencilComp]
            }
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            struct appdata {
                float4 vertex : POSITION;
            };
            struct v2f {
                float4 pos : SV_POSITION;
            };
            v2f vert(appdata v) {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            half4 frag(v2f i) : SV_Target {
                return half4(1,0,0,1);
            }
            ENDCG
        }
    } 
}
