using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Xraytool : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(GetComponent<Renderer>()){
            //GetComponent<Renderer>().material.renderQueue = 2000;
            GetComponent<Renderer>().material.SetInt("Stencil ID", 1) ;
            GetComponent<Renderer>().material.SetInt ("_StencilComp",6);
            GetComponent<Renderer>().material.SetInt("_StencilRef", 1) ;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
