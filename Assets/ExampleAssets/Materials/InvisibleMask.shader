﻿Shader "Custom/InvisibleMask"
{
    Properties
    {
        //_MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue"="Geometry-10"} // renders after all transparent objects (queue = 3001)
        Lighting off
        //相當於小於或者等於本身深度值時，該物體渲染
        ZTest LEqual
        //打開深度寫入
        ZWrite On
        //通道遮罩，為0時不寫入任何顏色通道，除了深度緩存
        ColorMask 0
        Pass
        {
        // Blend Zero One // makes this object transparent
        }
    }
}
